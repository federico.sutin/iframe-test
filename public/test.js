var drivechat = function (){
    document.getElementById("close").addEventListener("click",function(){sendEvent('close');})
    document.getElementById("minimize").addEventListener("click",function(){sendEvent('minimize');})
    document.getElementById("startChat").addEventListener("click",function(){sendEvent('startChat');})
    document.getElementById("messageSent").addEventListener("click",function(){sendEvent('messageSent');})
    document.getElementById("messageReceived").addEventListener("click",function(){sendEvent('messageReceived');})
}

function sendEvent(event){
    if (window.hasOwnProperty("mobileInterface")) {
        var strategy = strategies[event];
        if(strategy){
            strategy();
        }
    }
    else {
        parent.postMessage(event,"*");
    }
}

var strategies = {
    "close" : function(){ mobileInterface.driveChatClose();},
    "minimize" : function(){ mobileInterface.driveChatMinimise();},
    "startChat" : function(){ mobileInterface.driveChatStartChat();},
    "messageSent" : function(){ mobileInterface.driveChatMessageSent();},
    "messageReceived" : function(){ mobileInterface.driveChatMessageReceived();}
}
